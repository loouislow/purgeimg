#!/bin/bash
#
# @@script: purgeimg.sh
# @@description: Dig out corrupted image and tell where it's location with WARN message
# @@author: Loouis Low
# @@copyright: Goody Technologies
#

# Settings file is "purgeimg.conf"

source purgeimg.conf

function runas_root () {
    if [ "$(whoami &2>/dev/null)" != "root" ] && [ "$(id -un &2>/dev/null)" != "root" ]
        then
            echo -e "\e[34m[purgeimg]\e[39m permission denied."
        exit 1
    fi
}

function prerequisite() {
    if ! [ -x "$(command -v jpeginfo)" ]; then
       echo -e '\e[34m[purgeimg]\e[39m installing jpeginfo' >&2
       sudo apt-get install -y jpeginfo
    fi
}

function dig_bad_jpg() {
    echo -e "\e[34m[purgeimg]\e[39m digging JPG..."
    find "$target_dir" -name "$img_format_jpg" -exec jpeginfo -cv {} \; | grep -E "WARNING|ERROR"
}

function dig_bad_png() {
    echo -e "\e[34m[purgeimg]\e[39m digging PNG..."
    find "$target_dir" -name "$img_format_png" -exec jpeginfo -cv {} \; | grep -E "WARNING|ERROR"
}

function dig_bad_gif() {
    echo -e "\e[34m[purgeimg]\e[39m digging GIF..."
    find "$target_dir" -name "$img_format_gif" -exec jpeginfo -cv {} \; | grep -E "WARNING|ERROR"
}

### init
#runas_root
prerequisite
dig_bad_jpg
dig_bad_png
#dig_bad_gif
