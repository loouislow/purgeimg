# purgeimg

Dig out corrupted image and tell where it's location with WARN message

### Usage

To run,

```bash
$ bash purgeimg.sh
```

To configure,

```bash
$ gedit purgeimg.conf
```

---

Sorry, this is a quick and dirty script, I'll document it nicely soon.

The script is not fully tested.
